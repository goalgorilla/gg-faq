<?php
/**
 * @file
 * gg_faq.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function gg_faq_taxonomy_default_vocabularies() {
  return array(
    'faq' => array(
      'name' => 'FAQ',
      'machine_name' => 'faq',
      'description' => 'Categorieen voor veelgestelde vragen',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
